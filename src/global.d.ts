type IconProp = import('@fortawesome/fontawesome-svg-core').IconProp;
type ImageSourcePropType = import('react-native').ImageSourcePropType;

interface Word {
  _id: string;
  name: string;
  createdDate: string;
  wordDetails: Array<WordDetails>;
}

interface WordDetails {
  _id: string;
  word: string;
  definition: string;
  wordType: string;
  synonym: Array<any>;
  antonym: Array<any>;
  example: Array<any>;
}

interface Account {
  _id: string;
  username: string;
  email: string;
  avatar: string;
  displayName: string;
}

interface Exam {
  _id: string;
  name: string;
  topic: string;
  ques: Question[];
}

interface Question {
  _id: string;
  exam: string;
  name: string;
  answers: Answer[];
}

interface Answer {
  _id: string;
  question: string;
  answer: string;
}
