import React, {memo} from 'react';
import {
  StyleSheet,
  TouchableHighlight,
  TouchableHighlightProps,
  StyleProp,
  ViewStyle,
} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {IconProp} from '@fortawesome/fontawesome-svg-core';
import {useAppTheme} from "@configs/theme.config";

interface IconButtonProps extends TouchableHighlightProps {
  size?: number;
  icon: IconProp;
  iconColor?: string;
  onPress?: any;
  style?: StyleProp<ViewStyle>;
  underlayVisible?: boolean;
}

const IconButton = memo((props: IconButtonProps) => {
  const {colors} = useAppTheme();
  return (
    <TouchableHighlight
      underlayColor={colors.background3}
      {...props}
      style={[
        {
          alignItems: 'center',
          justifyContent: 'center',
          borderRadius: 99,
          padding: 8,
        },
        StyleSheet.flatten(props.style),
      ]}>
      <FontAwesomeIcon
        icon={props.icon}
        color={props.iconColor || colors.onBackground}
        size={props.size || 20}
      />
    </TouchableHighlight>
  );
});

export default IconButton;
