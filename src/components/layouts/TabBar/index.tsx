import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {Route} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {TouchableOpacity, View, Keyboard, Text} from 'react-native';
import {
  faBars,
  faEnvelope,
  faHome,
  faFolderTree,
} from '@fortawesome/free-solid-svg-icons';
import styles from './styles';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

export const CustomTabBar = ({state, descriptors, navigation}: any) => {
  const {bottom} = useSafeAreaInsets();

  const [visible, setVisible] = useState(true);

  useEffect(() => {
    const showSubscription = Keyboard.addListener('keyboardDidShow', () => {
      setVisible(false);
    });
    const hideSubscription = Keyboard.addListener('keyboardDidHide', () => {
      setVisible(true);
    });

    return () => {
      showSubscription.remove();
      hideSubscription.remove();
    };
  }, []);

  if (!visible) {
    return <View />;
  }
  return (
    <View style={[styles.contentBottomTab, {paddingBottom: bottom || 16}]}>
      {state.routes.map((route: Route<any>, index: number) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = async () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });
          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate({
              name: route.name,
              merge: true,
            });
          }
        };

        const color = isFocused ? '#000000' : 'grey';
        let icon = faHome;
        switch (route.name) {
          case 'Translation':
            icon = faHome;
            break;
        }
        return (
          <TouchableOpacity
            key={index}
            activeOpacity={0.8}
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            style={styles.itemTab}>
            <FontAwesomeIcon
              icon={icon}
              size={20}
              color={color}
              style={{
                marginBottom: 4,
              }}
            />
            <Text
              style={{
                color,
                fontSize: 11,
                marginTop: 4,
                fontWeight: isFocused ? '800' : '500',
              }}>
              {route.name}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};
