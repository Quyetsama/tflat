import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  contentBottomTab: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingTop: 16,
  },
  itemTab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
});
export default styles;
