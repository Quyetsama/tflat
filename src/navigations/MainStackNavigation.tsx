import {createNativeStackNavigator, NativeStackScreenProps} from '@react-navigation/native-stack';
import React from 'react';
import TranslationScreen from '@screens/Translation';
import DetailScreen from "@screens/Detail";
import SearchScreen from "@screens/Search";
import HomeScreen from "@screens/Home";
import SignIn from "@screens/Auth/SignIn";
import SignUp from "@screens/Auth/SignUp";
import AccountScreen from "@screens/Account";
import BookmarkScreen from "@screens/Bookmark";
import ExamsScreen from "@screens/Exams";
import DoExamScreen from "@screens/DoExam";

export type RootStackParamList = {
  Main: undefined;
  HomeScreen: undefined;
  TranslationScreen: undefined;
  DetailScreen: { _id: string };
  SearchScreen: undefined;
  SignInScreen: undefined;
  SignUpScreen: undefined;
  AccountScreen: undefined;
  BookmarkScreen: undefined;
  ExamsScreen: undefined;
  DoExamScreen: { exam: Exam };
};

export type StackNativeScreenProps<T extends keyof RootStackParamList> =
  NativeStackScreenProps<RootStackParamList, T>;

declare global {
  namespace ReactNavigation {
    interface ParamList extends RootStackParamList {}
  }
}

const Stack = createNativeStackNavigator<RootStackParamList>();

export const MainStackNavigation = () => {
  return (
    <Stack.Navigator initialRouteName={'HomeScreen'}>
      <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="TranslationScreen"
        component={TranslationScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="DetailScreen"
        component={DetailScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="SearchScreen"
        component={SearchScreen}
        options={{headerShown: false, animation: 'fade'}}
      />
      <Stack.Screen
        name="SignInScreen"
        component={SignIn}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="SignUpScreen"
        component={SignUp}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="AccountScreen"
        component={AccountScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="BookmarkScreen"
        component={BookmarkScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="ExamsScreen"
        component={ExamsScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="DoExamScreen"
        component={DoExamScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};
