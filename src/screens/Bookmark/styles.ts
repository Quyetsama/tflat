import {StyleSheet} from 'react-native';

const ueStyles = colors => StyleSheet.create({
  outer: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.primary,
  },
  title: {
    flex: 1,
    fontWeight: '700',
    fontSize: 20,
  },
  bookmark: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 16
  },
  text: {
    fontWeight: '500',
  },
});

export default ueStyles;
