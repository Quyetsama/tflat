import {FlatList, View} from "react-native";
import {useAppDispatch, useAppSelector} from "@redux/store";
import {useSafeAreaInsets} from "react-native-safe-area-context";
import {useAppTheme} from "@configs/theme.config";
import ueStyles from "@screens/Bookmark/styles";
import IconButton from "@components/common/IconButton";
import {faArrowLeft} from "@fortawesome/free-solid-svg-icons";
import {Text} from "react-native-paper";
import React from "react";

const BookmarkScreen = ({navigation}) => {
  const dispatch = useAppDispatch();
  const {top} = useSafeAreaInsets();
  const {colors} = useAppTheme();
  const styles = ueStyles(colors);
  const bookmarks = useAppSelector(state => state.auth.bookmarks);

  const renderItem = ({item}) => {
    return (
      <View key={item._id}
        style={styles.bookmark}>
        <Text style={styles.text}>{item.word.name}</Text>
        <Text style={styles.text}>{item.word.pronunciation}</Text>
      </View>
    );
  };

  return (
    <View style={styles.outer}>
      <View style={[styles.header, {paddingTop: top}]}>
        <IconButton icon={faArrowLeft}
          onPress={() => navigation.goBack()}
          style={{padding: 16}}
          underlayColor={'#FFFFFF22'} />
        <Text style={styles.title}>Từ vựng đã lưu</Text>
      </View>
      <FlatList data={bookmarks}
        renderItem={renderItem} />
    </View>
  );
};

export default BookmarkScreen;
