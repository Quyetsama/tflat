import {Alert, FlatList, View} from "react-native";
import {useAppDispatch} from "@redux/store";
import {useSafeAreaInsets} from "react-native-safe-area-context";
import {useAppTheme} from "@configs/theme.config";
import ueStyles from "@screens/DoExam/styles";
import IconButton from "@components/common/IconButton";
import {faArrowLeft} from "@fortawesome/free-solid-svg-icons";
import {ActivityIndicator, Button, RadioButton, Text} from "react-native-paper";
import React, {useEffect, useState} from "react";
import ApiService from "@services/api.service";

const DoExamScreen = ({route, navigation}) => {
  const dispatch = useAppDispatch();
  const {top} = useSafeAreaInsets();
  const {colors} = useAppTheme();
  const styles = ueStyles(colors);
  const [exam, setExam] = useState<Exam>(route.params.exam);
  const [answers, setAnswers] = useState<Omit<Answer, '_id'>[]>([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    (async function () {
      const data = await ApiService.getExamDetails(exam._id);
      setExam({...exam, ...data});
    })();
  }, []);


  const finishExam = async () => {
    try {
      setLoading(true);
      const answersId = answers.map(item => item.answer);
      const data = await ApiService.finishExam(exam._id, answersId);
      setLoading(false);
      Alert.alert('Result', data.result);
    } catch (e) {
      setLoading(false);
      console.log(e);
    }

  };


  const renderItem = ({item, index}: { item: Question, index: number }) => {
    const value = answers.find(e => e.question === item._id)?.answer || '';
    return (
      <View
        // pointerEvents={'none'}
        key={item._id}
        style={styles.questionOuter}>
        <Text style={styles.question}>Question: {item.name}</Text>
        <RadioButton.Group
          onValueChange={newValue => {
            const index = answers.findIndex(el => el.question === item._id);
            if (index >= 0) {
              setAnswers(prevState => {
                const newData = [...prevState];
                newData[index] = {
                  ...newData[index],
                  answer: newValue,
                };
                return newData;
              });
            } else {
              setAnswers(prevState => [...prevState, {answer: newValue, question: item._id}]);
            }
          }}
          value={value}>
          {item.answers.map(answer => (
            <View key={answer._id}
              style={styles.answer}>
              <RadioButton.Android value={answer._id} />
              <Text>{answer.answer}</Text>
            </View>
          ))}
        </RadioButton.Group>
      </View>
    );
  };

  return (
    <View style={styles.outer}>
      <View style={[styles.header, {paddingTop: top}]}>
        <IconButton icon={faArrowLeft}
          onPress={() => navigation.goBack()}
          style={{padding: 16}}
          underlayColor={'#FFFFFF22'} />
        <Text style={styles.title}>{exam.topic}</Text>
        <Text style={styles.total}>Total: {answers.length}/{exam.ques.length}</Text>
      </View>
      <View style={styles.content}>
        <FlatList
          contentContainerStyle={{flexGrow: 1, gap: 24, padding: 16, paddingBottom: 32}}
          data={exam.ques}
          renderItem={renderItem}
          ListFooterComponent={
            exam.ques.length && answers.length === exam.ques.length ? <Button disabled={loading}
              loading={loading}
              mode={'contained'}
              onPress={finishExam}>Finish</Button> : <></>
          }
          ListEmptyComponent={<View style={{
            flex: 1,
            justifyContent: 'center',
          }}><ActivityIndicator color={colors.onBackground} /></View>}
        />
      </View>
    </View>
  );
};

export default DoExamScreen;
