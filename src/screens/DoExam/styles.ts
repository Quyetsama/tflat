import {StyleSheet} from 'react-native';

const ueStyles = colors => StyleSheet.create({
  outer: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.primary,
  },
  title: {
    flex: 1,
    fontWeight: '700',
    fontSize: 20,
  },
  index: {
    padding: 16,
  },
  total: {
    textAlign: 'right',
    padding: 16
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    gap: 12,
  },
  name: {
    textAlign: 'center',
    fontWeight: '500',
    fontSize: 20,
    paddingBottom: 24
  },
  row: {
    flexDirection: 'row',
    gap: 12,
  },
  questionOuter: {
    gap: 12
  },
  question: {
  },
  answer: {
    flexDirection: 'row',
    alignItems: 'center'
  }
});

export default ueStyles;
