import {View} from "react-native";
import {useSafeAreaInsets} from "react-native-safe-area-context";
import {useAppTheme} from "@configs/theme.config";
import ueStyles from "@screens/Account/styles";
import IconButton from "@components/common/IconButton";
import {faArrowLeft} from "@fortawesome/free-solid-svg-icons";
import {Button, Text} from "react-native-paper";
import React from "react";
import {useAppDispatch} from "@redux/store";
import {setLogout} from "@redux/slices/auth.slice";

const AccountScreen = ({navigation}) => {
  const dispatch = useAppDispatch();
  const {top} = useSafeAreaInsets();
  const {colors} = useAppTheme();
  const styles = ueStyles(colors);

  return (
    <View style={styles.outer}>
      <View style={[styles.header, {paddingTop: top}]}>
        <IconButton icon={faArrowLeft}
          onPress={() => navigation.goBack()}
          style={{padding: 16}}
          underlayColor={'#FFFFFF22'} />
        <Text style={styles.title}>Tài khoản</Text>
      </View>
      <View>
        <Button onPress={() => {
          dispatch(setLogout());
          navigation.goBack();
        }}>Đăng xuất</Button>
      </View>
    </View>
  );
};

export default AccountScreen;
