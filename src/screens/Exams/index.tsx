import {FlatList, TouchableOpacity, View} from "react-native";
import {useAppDispatch} from "@redux/store";
import {useSafeAreaInsets} from "react-native-safe-area-context";
import {useAppTheme} from "@configs/theme.config";
import ueStyles from "@screens/Exams/styles";
import IconButton from "@components/common/IconButton";
import {faArrowLeft} from "@fortawesome/free-solid-svg-icons";
import {Text} from "react-native-paper";
import React, {useEffect, useState} from "react";
import ApiService from "@services/api.service";

const ExamsScreen = ({navigation}) => {
  const dispatch = useAppDispatch();
  const {top} = useSafeAreaInsets();
  const {colors} = useAppTheme();
  const styles = ueStyles(colors);
  const [exams, setExams] = useState<Exam[]>([]);

  useEffect(() => {
    (async function () {
      const data = await ApiService.getExams();
      setExams(data);
    })();
  }, []);

  const renderItem = ({item}) => {
    return (
      <TouchableOpacity
        key={item._id}
        onPress={() => navigation.navigate('DoExamScreen', {exam: item})}
        activeOpacity={0.6}
        style={styles.exam}>
        <>
          <Text numberOfLines={1}
            style={styles.label}>{item.name}</Text>
          <Text numberOfLines={1}
            style={styles.topic}>{item.topic}</Text>
        </>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.outer}>
      <View style={[styles.header, {paddingTop: top}]}>
        <IconButton icon={faArrowLeft}
          onPress={() => navigation.goBack()}
          style={{padding: 16}}
          underlayColor={'#FFFFFF22'} />
        <Text style={styles.title}>Trắc nghiệm</Text>
      </View>
      <FlatList
        contentContainerStyle={{gap: 12, padding: 16}}
        data={exams}
        renderItem={renderItem}
      />
    </View>
  );
};

export default ExamsScreen;
