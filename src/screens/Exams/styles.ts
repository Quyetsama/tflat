import {StyleSheet} from 'react-native';

const ueStyles = colors => StyleSheet.create({
  outer: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.primary,
  },
  title: {
    flex: 1,
    fontWeight: '700',
    fontSize: 20,
  },
  exam: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 16,
    backgroundColor: colors.background2,
    borderRadius: 8,
    gap: 12,
  },
  label: {
    flex: 1,
    fontWeight: '700',
  },
  topic: {
    fontWeight: '500',
    color: colors.text + 88,
  },
});

export default ueStyles;
