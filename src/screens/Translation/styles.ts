import {StyleSheet} from 'react-native';

const ueStyles = colors => StyleSheet.create({
  outer: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: 16,
    backgroundColor: colors.primary,
  },
  title: {
    fontWeight: '700',
    fontSize: 20,
  },
  inputOuter: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    backgroundColor: colors.background2,
    borderRadius: 4,
    padding: 8,

  },
  input: {
    flex: 1,
    minHeight: 120,
    textAlignVertical: 'top',
    padding: 8,
    color: colors.text
  },
  button: {
    flex: 1,
    backgroundColor: colors.primary,
    alignItems: 'center',
    padding: 8,
    borderRadius: 4
  }
});

export default ueStyles;
