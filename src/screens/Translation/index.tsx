import React, {useEffect, useState} from 'react';
import {ActivityIndicator, Text} from 'react-native-paper';
import {useSafeAreaInsets} from "react-native-safe-area-context";
import {useAppTheme} from "@configs/theme.config";
import ueStyles from "@screens/Translation/styles";
import {Keyboard, ScrollView, TextInput, TouchableOpacity, TouchableWithoutFeedback, View} from "react-native";
import IconButton from "@components/common/IconButton";
import {faArrowLeft, faClose, faCopy, faVolumeLow} from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import Tts from 'react-native-tts';
import Clipboard from "@react-native-clipboard/clipboard";
import {Toast} from "@utils/index";

const TranslationScreen = ({navigation}) => {
  const {top} = useSafeAreaInsets();
  const {colors} = useAppTheme();
  const styles = ueStyles(colors);
  const [text, setText] = useState('');
  const [translated, setTranslated] = useState('');
  const [loading, setLoading] = useState(false);
  const [mode, setMode] = useState<'en|vi' | 'vi|en'>('en|vi');

  useEffect(() => {
    Tts.addEventListener('tts-start', (event) => console.log("start", event));
    Tts.addEventListener('tts-finish', (event) => console.log("finish", event));
    Tts.addEventListener('tts-cancel', (event) => console.log("cancel", event));

    return () => {
      Tts.removeAllListeners('tts-start');
      Tts.removeAllListeners('tts-finish');
      Tts.removeAllListeners('tts-cancel');
    };
  }, []);

  const doTranslate = async (langpair: 'en|vi' | 'vi|en') => {
    try {
      if (!text.trim()) return;
      setMode(langpair);
      if (langpair === 'en|vi') {
        await Tts.setDefaultLanguage('vi-VN');
      } else {
        await Tts.setDefaultLanguage('en-US');
      }

      setLoading(true);
      const options = {
        method: 'GET',
        url: 'https://api.mymemory.translated.net/get',
        params: {q: text, langpair: langpair},
      };
      const {data} = await axios.request(options);
      setTranslated(data.responseData.translatedText);
      setLoading(false);
    } catch (e) {
      console.log(e);
      setLoading(false);
    }
  };

  return (
    <View style={styles.outer}>
      <TouchableWithoutFeedback style={{flex: 1}}
        onPress={Keyboard.dismiss}>
        <View style={{flex: 1}}>
          <View style={[styles.header, {paddingTop: top}]}>
            <IconButton icon={faArrowLeft}
              onPress={() => navigation.goBack()}
              style={{padding: 16}}
              underlayColor={'#FFFFFF22'} />
            <Text style={styles.title}>Dịch văn bản</Text>
          </View>
          <ScrollView contentContainerStyle={{padding: 16, rowGap: 16}}
            keyboardShouldPersistTaps={'always'}>
            <View style={styles.inputOuter}>
              <TextInput
                placeholder={'Nhập cụm từ, câu văn hoặc đoạn văn để dịch.'}
                placeholderTextColor={colors.text + 66}
                style={styles.input}
                multiline={true}
                selectionColor={colors.primary}
                cursorColor={colors.primary}
                value={text}
                onChangeText={text => setText(text)}
              />
              <View style={{gap: 12}}>
                <IconButton icon={faClose}
                  onPress={() => setText('')} />
                <IconButton icon={faVolumeLow}
                  onPress={async () => {
                    if (mode === 'en|vi') {
                      await Tts.setDefaultLanguage('en');
                    } else {
                      await Tts.setDefaultLanguage('vi');
                    }
                    Tts.speak(text);
                  }} />
                {
                  loading && (
                    <ActivityIndicator size={20} />
                  )
                }
              </View>
            </View>
            <View style={{flexDirection: 'row', columnGap: 12}}>
              <TouchableOpacity
                disabled={loading}
                style={styles.button}
                activeOpacity={0.6}
                onPress={() => doTranslate('en|vi')}
              >
                <Text>Anh - Việt</Text>
              </TouchableOpacity>
              <TouchableOpacity
                disabled={loading}
                style={styles.button}
                activeOpacity={0.6}
                onPress={() => doTranslate('vi|en')}
              >
                <Text>Việt - Anh</Text>
              </TouchableOpacity>
            </View>
            {translated && (
              <>
                <View style={styles.inputOuter}>
                  <TextInput
                    editable={false}
                    placeholder={'Results.'}
                    placeholderTextColor={colors.text + 66}
                    style={styles.input}
                    multiline={true}
                    selectionColor={colors.primary}
                    cursorColor={colors.primary}
                    value={translated}
                  />
                  <View style={{gap: 12}}>
                    <IconButton icon={faCopy}
                      onPress={() => {
                        Clipboard.setString(translated);
                        Toast('Copied');
                      }} />
                    <IconButton icon={faVolumeLow}
                      onPress={async () => {
                        if (mode === 'en|vi') {
                          await Tts.setDefaultLanguage('vi-VN');
                        } else {
                          await Tts.setDefaultLanguage('en-US');
                        }
                        Tts.speak(translated);
                      }} />
                  </View>
                </View>
                {/*<TouchableOpacity*/}
                {/*  style={styles.button}*/}
                {/*  activeOpacity={0.6}*/}
                {/*  onPress={() => navigation.navigate('DetailScreen')}*/}
                {/*>*/}
                {/*  <Text>Xem chi tiết</Text>*/}
                {/*</TouchableOpacity>*/}
              </>
            )}
          </ScrollView>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

export default TranslationScreen;
