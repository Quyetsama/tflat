import React from 'react';
import {Text} from 'react-native-paper';
import {useSafeAreaInsets} from "react-native-safe-area-context";
import {useAppTheme} from "@configs/theme.config";
import ueStyles from "@screens/Home/styles";
import {Keyboard, ScrollView, TextInput, TouchableOpacity, TouchableWithoutFeedback, View} from "react-native";
import {faChevronRight, faSearch, faUserCircle} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import IconButton from "@components/common/IconButton";
import {useAppSelector} from "@redux/store";
import ExamsScreen from "@screens/Exams";

const HomeScreen = ({navigation}) => {
  const {top} = useSafeAreaInsets();
  const {colors} = useAppTheme();
  const styles = ueStyles(colors);
  const isSignedIn = useAppSelector(state => state.auth.isSignedIn);

  const data: any[] = [
    {
      label: 'Dịch văn bản',
      onPress: () => navigation.navigate('TranslationScreen'),
    },
    {
      label: 'Từ vựng đã lưu',
      signIn: true,
      onPress: () => navigation.navigate('BookmarkScreen'),
    },
    {
      label: 'Trắc nghiệm',
      signIn: true,
      onPress: () => navigation.navigate('ExamsScreen'),
    },
  ];

  return (
    <View style={styles.outer}>
      <TouchableWithoutFeedback style={{flex: 1}}
        onPress={Keyboard.dismiss}>
        <View style={{flex: 1}}>
          <View style={[styles.header, {paddingTop: top}]}>
            <Text style={styles.title}>TFlat</Text>
            <IconButton icon={faUserCircle}
              onPress={() => {
                if (isSignedIn) {
                  navigation.navigate('AccountScreen');
                } else {
                  navigation.navigate('SignInScreen');
                }
              }}
              style={{padding: 16}}
              underlayColor={'#FFFFFF22'} />
          </View>
          <ScrollView contentContainerStyle={{padding: 16, rowGap: 16}}
            keyboardShouldPersistTaps={'always'}>
            <TouchableOpacity style={styles.inputOuter}
              activeOpacity={0.6}
              onPress={() => navigation.navigate('SearchScreen')}>
              <View pointerEvents={'none'}
                style={{flex: 1}}>
                <TextInput placeholder={'Tra từ điển'}
                  placeholderTextColor={colors.text + 88}
                  style={styles.input} />
              </View>
              <FontAwesomeIcon icon={faSearch}
                color={colors.onBackground}
                size={16} />
            </TouchableOpacity>
            {data.map((item, index) => {
              return (
                <TouchableOpacity key={index.toString()}
                  style={styles.inputOuter}
                  activeOpacity={0.6}
                  onPress={() => {
                    if (item.signIn) {
                      if (isSignedIn) {
                        item.onPress();
                      } else {
                        navigation.navigate('SignInScreen');
                      }
                    } else {
                      item.onPress();
                    }
                  }}
                >
                  <Text style={{
                    flex: 1,
                  }}>{item.label}</Text>
                  <FontAwesomeIcon icon={faChevronRight}
                    color={colors.onBackground}
                    size={14} />
                </TouchableOpacity>
              );
            })}
          </ScrollView>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

export default HomeScreen;
