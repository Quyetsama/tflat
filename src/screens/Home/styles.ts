import {StyleSheet} from 'react-native';

const ueStyles = colors => StyleSheet.create({
  outer: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 16,
    backgroundColor: colors.primary
  },
  title: {
    fontWeight: '700',
    fontSize: 20,
  },
  inputOuter: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.background2,
    borderRadius: 8,
    paddingHorizontal: 16,
    height: 48
  },
  input: {
    color: colors.text
  },
  button: {
    flex: 1,
    backgroundColor: colors.primary,
    alignItems: 'center',
    padding: 8,
    borderRadius: 4
  }
});

export default ueStyles;
