import {ScrollView, View} from "react-native";
import IconButton from "@components/common/IconButton";
import {faArrowLeft, faEnvelope, faLock, faUser} from "@fortawesome/free-solid-svg-icons";
import {Button, Text} from "react-native-paper";
import React, {useState} from "react";
import {useAppTheme} from "@configs/theme.config";
import ueStyles from "@screens/Auth/styles";
import {useSafeAreaInsets} from "react-native-safe-area-context";
import AuthInput from "@screens/Auth/components/AuthInput";
import ApiService from "@services/api.service";
import {Toast} from "@utils/index";

const SignUp = ({navigation}) => {
  const {top, bottom} = useSafeAreaInsets();
  const {colors} = useAppTheme();
  const styles = ueStyles(colors);
  const [authData, setAuthData] = useState({
    username: '',
    email: '',
    password: '',
  });
  const [loading, setLoading] = useState(false);

  const doRegister = async () => {
    try {
      setLoading(true);
      await ApiService.signUp(authData.username, authData.email, authData.password);
      Toast('Đăng ký thành công');
      setAuthData({
        username: '',
        email: '',
        password: '',
      });
      setLoading(false);
    } catch (e: any) {
      console.log(e.response.data);
      Toast(`${e?.response?.data?.message || 'Có lỗi xảy ra'}`);
      setLoading(false);
    }
  };

  return (
    <View style={styles.outer}>
      <View style={[styles.header, {paddingTop: top}]}>
        <IconButton icon={faArrowLeft}
          onPress={() => navigation.goBack()}
          style={{padding: 16}}
          underlayColor={'#FFFFFF22'} />
        <Text style={styles.title}>Đăng ký</Text>
      </View>
      <View style={styles.body}>
        <ScrollView contentContainerStyle={{gap: 16, padding: 16}}>
          <AuthInput
            icon={faUser}
            placeholder={'Tên đăng nhập'}
            value={authData.username}
            onChangeText={text => setAuthData({...authData, username: text})}
          />
          <AuthInput
            icon={faEnvelope}
            placeholder={'example@gmail.com'}
            value={authData.email}
            onChangeText={text => setAuthData({...authData, email: text})}
          />
          <AuthInput
            icon={faLock}
            placeholder={'Mật khẩu'}
            secureTextEntry
            value={authData.password}
            onChangeText={text => setAuthData({...authData, password: text})}
          />
          <Button loading={loading} disabled={loading} style={{borderRadius: 4}}
            mode={'contained'}
            onPress={doRegister}>
            Đăng ký
          </Button>
        </ScrollView>
        <Text style={{
          textAlign: 'center',
          marginBottom: bottom || 16,
        }}>Bạn đã có tài khoản? <Text style={{color: colors.primary}}
          onPress={() => navigation.navigate('SignInScreen')}>Đăng nhập</Text></Text>
      </View>
    </View>
  );
};

export default SignUp;
