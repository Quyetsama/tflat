import {StyleSheet} from 'react-native';

const ueStyles = colors => StyleSheet.create({
  outer: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.primary,
  },
  title: {
    flex: 1,
    fontWeight: '700',
    fontSize: 20,
  },
  body: {
    flex: 1,
  }
});

export default ueStyles;
