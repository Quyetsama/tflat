import {StyleSheet, TextInput, TextInputProps, View, ViewStyle} from "react-native";
import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import {useAppTheme} from "@configs/theme.config";

interface AuthInputProps extends TextInputProps {
  icon: IconProp;
  containerStyle?: ViewStyle;
}

const AuthInput = (props: AuthInputProps) => {
  const {colors} = useAppTheme();

  return (
    <View style={[{
      flexDirection: 'row',
      alignItems: 'center',
      gap: 12,
      backgroundColor: colors.background2,
      paddingHorizontal: 16,
      borderRadius: 4,
    }, StyleSheet.flatten(props.containerStyle)]}>
      <FontAwesomeIcon icon={props.icon}
        color={colors.onSurface}
        size={20} />
      <TextInput
        placeholder={'Email'}
        placeholderTextColor={colors.text + 66}
        cursorColor={colors.primary}
        selectionColor={colors.primary}
        {...props}
        style={[{
          flex: 1,
          height: 48,
          color: colors.text,
        }, StyleSheet.flatten(props.style)]}
      />
    </View>
  );
};

export default AuthInput;
