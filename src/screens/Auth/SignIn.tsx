import {ScrollView, View} from "react-native";
import IconButton from "@components/common/IconButton";
import {faArrowLeft, faLock, faUser} from "@fortawesome/free-solid-svg-icons";
import {Button, Text} from "react-native-paper";
import React, {useState} from "react";
import {useAppTheme} from "@configs/theme.config";
import ueStyles from "@screens/Auth/styles";
import {useSafeAreaInsets} from "react-native-safe-area-context";
import AuthInput from "@screens/Auth/components/AuthInput";
import {Toast} from "@utils/index";
import {useAppDispatch} from "@redux/store";
import {doSignInWithPassword} from "@redux/actions/auth.action";

const SignIn = ({navigation}) => {
  const dispatch = useAppDispatch();
  const {top, bottom} = useSafeAreaInsets();
  const {colors} = useAppTheme();
  const styles = ueStyles(colors);
  const [authData, setAuthData] = useState({
    username: '',
    password: '',
  });
  const [loading, setLoading] = useState(false);

  const doSignIn = async () => {
    try {
      setLoading(true);
      const res = await dispatch(doSignInWithPassword(authData));
      if (res.meta.requestStatus === 'fulfilled') {
        navigation.reset({
          index: 0,
          routes: [
            {
              name: 'HomeScreen',
            },
          ],
        })
      } else {
        Toast(`Có lỗi xảy ra`);
      }
      setLoading(false);
    } catch (e: any) {
      console.log(e);
      Toast(`Có lỗi xảy ra`);
      setLoading(false);
    }
  };

  return (
    <View style={styles.outer}>
      <View style={[styles.header, {paddingTop: top}]}>
        <IconButton icon={faArrowLeft}
          onPress={() => navigation.goBack()}
          style={{padding: 16}}
          underlayColor={'#FFFFFF22'} />
        <Text style={styles.title}>Đăng nhập</Text>
      </View>
      <View style={styles.body}>
        <ScrollView contentContainerStyle={{gap: 16, padding: 16}}>
          <AuthInput icon={faUser}
            placeholder={'Tên đăng nhập'}
            value={authData.username}
            onChangeText={text => setAuthData({...authData, username: text})}
          />
          <AuthInput icon={faLock}
            placeholder={'Mật khẩu'}
            secureTextEntry
            value={authData.password}
            onChangeText={text => setAuthData({...authData, password: text})}
          />
          <Button
            loading={loading}
            disabled={loading}
            style={{borderRadius: 4}}
            mode={'contained'}
            onPress={doSignIn}>
            Đăng nhập
          </Button>
        </ScrollView>
        <Text style={{
          textAlign: 'center',
          marginBottom: bottom || 16,
        }}>Bạn chưa có tài khoản? <Text style={{color: colors.primary}}
          onPress={() => navigation.navigate('SignUpScreen')}>Đăng ký</Text></Text>
      </View>
    </View>
  );
};

export default SignIn;
