import {StyleSheet} from 'react-native';

const ueStyles = colors => StyleSheet.create({
  outer: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.primary,
  },
  title: {
    flex: 1,
    fontWeight: '700',
    fontSize: 20,
  },
  nameOuter: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 16,
    paddingRight: 8,
    gap: 12
  },
  name: {
    flex: 1,
    fontWeight: '700',
    fontSize: 16
  },
  section: {
    paddingHorizontal: 16,
    gap: 12
  },
  wordType: {
    fontWeight: '600',
    fontSize: 16,
    textDecorationLine: 'underline',
  },
  definition: {
    color: '#3498db'
  }
});

export default ueStyles;
