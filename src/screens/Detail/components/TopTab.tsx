import {StyleSheet, View} from "react-native";
import TabItem from "@screens/Detail/components/TabItem";
import {useAppTheme} from "@configs/theme.config";
import React from "react";

export const tabs = [
  'Anh - Việt',
  'Đồng nghĩa',
  'Ghi chú',
];

interface TopTabProps {
  tab: number;
  setTab: React.Dispatch<React.SetStateAction<number>>;
}

const TopTab = ({tab, setTab}: TopTabProps) => {
  const {colors} = useAppTheme();
  const styles = useStyles(colors);

  return (
    <View style={styles.outer}>
      {tabs.map((item, index) => (
          <TabItem key={index.toString()}
            label={item}
            selected={index === tab}
            onPress={() => {
              setTab(index);
            }} />
        ),
      )}
    </View>
  );
};

const useStyles = colors => StyleSheet.create({
  outer: {
    flexDirection: 'row',
    backgroundColor: colors.primary,
  },
});

export default TopTab;
