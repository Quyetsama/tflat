import {StyleSheet, TouchableOpacity, TouchableOpacityProps, View} from "react-native";
import {Text} from "react-native-paper";
import {useAppTheme} from "@configs/theme.config";

interface TabItemProps extends TouchableOpacityProps {
  label: string;
  selected: boolean;
}

const TabItem = (props: TabItemProps) => {
  const {colors} = useAppTheme();
  const styles = useStyles(colors);

  return (
    <TouchableOpacity
      {...props}
      activeOpacity={0.6}
      style={[styles.outer, StyleSheet.flatten(props.style)]}
    >
      <Text style={[styles.label, {fontWeight: props.selected ? '700' : '500'}]}>{props.label.toUpperCase()}</Text>
      {props.selected && (
        <View style={styles.indicator} />
      )}
    </TouchableOpacity>
  );
};

const useStyles = colors => StyleSheet.create({
  outer: {
    flex: 1,
    alignItems: "center",
    backgroundColor: colors.primary,
    padding: 8,
    paddingBottom: 16,
  },
  label: {
    fontWeight: '500',
  },
  indicator: {
    position: 'absolute',
    backgroundColor: colors.onPrimary,
    bottom: 0,
    left: 0,
    right: 0,
    height: 4,
  },
});

export default TabItem;
