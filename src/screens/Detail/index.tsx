import React, {useEffect, useMemo, useState} from 'react';
import {ActivityIndicator, Text} from 'react-native-paper';
import {useSafeAreaInsets} from "react-native-safe-area-context";
import {useAppTheme} from "@configs/theme.config";
import ueStyles from "@screens/Detail/styles";
import {ScrollView, View} from "react-native";
import IconButton from "@components/common/IconButton";
import {faArrowLeft, faHeart as faHeartS, faVolumeDown} from "@fortawesome/free-solid-svg-icons";
import {faHeart} from "@fortawesome/free-regular-svg-icons";
import Tts from "react-native-tts";
import ApiService from "@services/api.service";
import {useAppDispatch, useAppSelector} from "@redux/store";
import {loadBookmarks} from "@redux/actions/boot.action";

function capitalize(s: string) {
  return s[0].toUpperCase() + s.slice(1);
}

const DetailScreen = ({route, navigation}) => {
  const dispatch = useAppDispatch();
  const {top} = useSafeAreaInsets();
  const {colors} = useAppTheme();
  const styles = ueStyles(colors);
  const [tab, setTab] = useState(0);
  const [loading, setLoading] = useState(false);
  const [word, setWord] = useState<Word | undefined>();
  const bookmarks = useAppSelector(state => state.auth.bookmarks);
  const bookmarkId = useMemo(() => bookmarks.map(bookmark => bookmark.word).find(item => item._id === word?._id)?._id, [bookmarks, word]);

  useEffect(() => {
    doQuery().then();
    Tts.setDefaultLanguage('en').then();
    Tts.addEventListener('tts-start', (event) => console.log("start", event));
    Tts.addEventListener('tts-finish', (event) => console.log("finish", event));
    Tts.addEventListener('tts-cancel', (event) => console.log("cancel", event));

    return () => {
      Tts.removeAllListeners('tts-start');
      Tts.removeAllListeners('tts-finish');
      Tts.removeAllListeners('tts-cancel');
    };
  }, []);

  const doQuery = async () => {
    try {
      setLoading(true);
      const {data} = await ApiService.wordDetails(route.params._id);
      setWord(data);
      setLoading(false);
    } catch (e) {
      console.log(e);
      setLoading(false);
    }
  };
  console.log(bookmarkId);
  const changeBookmark = async () => {
    try {
      if (bookmarkId) {
        await ApiService.removeBookmark(bookmarkId);
      } else {
        await ApiService.addBookmark(word?._id || route.params._id);
      }
      dispatch(loadBookmarks());
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <View style={styles.outer}>
          <View style={[styles.header, {paddingTop: top}]}>
            <IconButton icon={faArrowLeft}
              onPress={() => navigation.goBack()}
              style={{padding: 16}}
              underlayColor={'#FFFFFF22'} />
            <Text style={styles.title}>Chi tiết</Text>
            <IconButton icon={bookmarkId ? faHeartS : faHeart}
              onPress={changeBookmark}
              style={{padding: 16}}
              underlayColor={'#FFFFFF22'} />
          </View>
      {/*<TopTab tab={tab}*/}
      {/*  setTab={setTab} />*/}
      {word && (
        <View style={{flex: 1}}>
          {loading ? (
            <View style={{
              flex: 1,
              justifyContent: 'center',
            }}>
              <ActivityIndicator color={colors.onBackground}
                size={'large'} />
            </View>
          ) : (
            <ScrollView contentContainerStyle={{flexGrow: 1, rowGap: 16}}>
              <View style={styles.nameOuter}>
                <Text style={styles.name}>{capitalize(word.name.replace(/[^a-zA-Z ]/g, ""))}</Text>
                <IconButton icon={faVolumeDown}
                  onPress={async () => {
                    Tts.speak(word.name.replace(/[^a-zA-Z ]/g, ""));
                  }} />
              </View>
              {word.wordDetails.map((item) => {
                return (
                  <View key={item._id}
                    style={styles.section}>
                    <Text style={styles.wordType}>{capitalize(item.wordType)}</Text>
                    <Text style={styles.definition}>{item.definition}</Text>
                  </View>
                );
              })}
            </ScrollView>
          )}
        </View>
      )}
    </View>
  );
};

export default DetailScreen;
