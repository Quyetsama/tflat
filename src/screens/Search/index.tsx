import {ScrollView, TextInput, TouchableOpacity, View} from "react-native";
import {Text} from "react-native-paper";
import {useAppTheme} from "@configs/theme.config";
import ueStyles from "@screens/Search/styles";
import {useSafeAreaInsets} from "react-native-safe-area-context";
import SearchItem from "@screens/Search/SearchItem";
import {useEffect, useState} from "react";
import ApiService from "@services/api.service";

let myTimeOut: number;

const SearchScreen = ({navigation}) => {
  const {top} = useSafeAreaInsets();
  const {colors} = useAppTheme();
  const styles = ueStyles(colors);
  const [query, setQuery] = useState('');
  const [suggestion, setSuggestion] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    myTimeOut = setTimeout(doQuery, 500);

    return () => {
      if (myTimeOut) {
        clearTimeout(myTimeOut);
      }
    };
  }, [query]);

  const doQuery = async () => {
    try {
      if (!query.trim()) return;
      setLoading(true);
      const {data} = await ApiService.search(query);
      setSuggestion(data);
      setLoading(false);
    } catch (e) {
      console.log(e);
      setLoading(false);
    }
  };

  return (
    <View style={styles.outer}>
      <View style={[styles.header, {paddingTop: top}]}>
        <TextInput
          value={query}
          onChangeText={text => setQuery(text)}
          placeholder={'Search ...'}
          placeholderTextColor={colors.text + 88}
          style={styles.input} />
        <TouchableOpacity style={styles.cancelBtn}
          onPress={() => navigation.goBack()}>
          <Text>Cancel</Text>
        </TouchableOpacity>
      </View>
      <View style={{flex: 1}}>
        <ScrollView>
          {suggestion.map((item: any) => {
            return <SearchItem key={item.id}
              suggestion={item} />;
          })}
        </ScrollView>
      </View>
    </View>
  );
};

export default SearchScreen;
