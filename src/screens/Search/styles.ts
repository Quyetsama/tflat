import {StyleSheet} from 'react-native';

const ueStyles = colors => StyleSheet.create({
  outer: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 16,
    paddingBottom: 8,
    gap: 12
    // backgroundColor: colors.primary
  },
  input: {
    flex: 1,
    backgroundColor: colors.background2,
    paddingHorizontal: 12,
    minHeight: 48,
    borderRadius: 8,
    color: colors.text
  },
  cancelBtn: {
    height: 48,
    justifyContent: 'center'
  }
});

export default ueStyles;
