import {TouchableHighlight} from "react-native";
import {Text} from "react-native-paper";
import {useAppTheme} from "@configs/theme.config";
import {useNavigation} from "@react-navigation/native";

const SearchItem = ({suggestion}: { suggestion: any }) => {
  const navigation = useNavigation();
  const {colors} = useAppTheme();

  return (
    <TouchableHighlight
      underlayColor={colors.primary}
      onPress={() => {
        navigation.navigate('DetailScreen', {_id: suggestion.id});
      }}
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
      padding: 16
    }}>
      <>
        <Text style={{
          fontWeight: '500',
          fontSize: 16
        }}>{suggestion.name.replace(/[^a-zA-Z ]/g, "")}</Text>
        {/*<Text style={{*/}
        {/*  fontWeight: '500',*/}
        {/*  fontSize: 16*/}
        {/*}}>{suggestion.pronunciation?.replace('\n', '')}</Text>*/}
      </>
    </TouchableHighlight>
  );
};

export default SearchItem;
