import React, {useEffect} from 'react';
import {Platform, StatusBar, UIManager} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {MainStackNavigation} from '@navigations/MainStackNavigation';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {Provider} from 'react-redux';
import {persistor, store, useAppDispatch} from '@redux/store';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider as PaperProvider} from 'react-native-paper';
import {navigationTheme, paperTheme} from '@configs/theme.config';
import {bootApp} from "@redux/actions/boot.action";

if (
  Platform.OS === 'android' &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

const AppInner = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(bootApp());
  }, []);

  return (
    <GestureHandlerRootView style={{flex: 1}}>
      <PaperProvider theme={paperTheme as any}>
        <NavigationContainer theme={navigationTheme}>
          <StatusBar
            translucent
            barStyle="light-content"
            backgroundColor="transparent"
          />
          <MainStackNavigation />
        </NavigationContainer>
      </PaperProvider>
    </GestureHandlerRootView>
  );
};

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <AppInner />
      </PersistGate>
    </Provider>
  );
};

export default App;
