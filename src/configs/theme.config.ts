import {DefaultTheme as PaperDefaultTheme, useTheme} from 'react-native-paper';
import {DefaultTheme as NavigationTheme, Theme} from '@react-navigation/native';

const colors = {
  primary: '#0056A5',
  onPrimary: '#FFFFFF',
  secondary: '#6BE48B',
  background: '#121212',
  background2: '#272727',
  background3: '#1A1B20',
  button: '#242526',
  onBackground: '#FFFFFF',
  title: '#FFFFFF',
  label: '#FFFFFF',
  text: '#FFFFFF',
  danger: '#ff443a',
  grey1: '#8e8e93',
  grey2: '#636366',
  grey3: '#48484a',
  border: '#8e8e93',
  border2: '#636366',
  surface: '#121212',
  onSurface: '#FFFFFF',
  onSurfaceDisabled: '#8e8e93',
};

export const paperTheme = {
  ...PaperDefaultTheme,
  roundness: 4,
  version: 3,
  colors: {
    ...PaperDefaultTheme.colors,
    ...colors,
  },
};

export const navigationTheme: Theme = {
  ...NavigationTheme,
  colors: {
    ...NavigationTheme.colors,
    background: colors.background,
  },
};

export type PaperThemeOverride = typeof paperTheme;
export type PaperThemeColorsOverride = typeof paperTheme.colors;
export const useAppTheme = () => useTheme<PaperThemeOverride>();
