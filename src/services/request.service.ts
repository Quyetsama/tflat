import axios, {AxiosRequestConfig} from 'axios/index';
import {RootState, store} from '@redux/store';

export class RequestService {
  axiosInstance = axios.create({});

  constructor() {
    this.axiosInstance.interceptors.response.use(
      response => {
        return response;
      },
      error => {
        // if (error.response.status === 401) {
        //   return store.dispatch(refreshToken()).then(() => {
        //     const {tokens} = store.getState()?.auth as RootState['auth'];
        //     if (tokens) {
        //       error.config.headers.Authorization = `Bearer ${tokens.access.token}`;
        //       return this.axiosInstance(error.config);
        //     }
        //   });
        // }
        return Promise.reject(error);
      },
    );
  }

  async callApi(
    endpoint: string,
    config: AxiosRequestConfig = {method: 'GET'},
  ) {
    const {access_token} = store.getState().auth as RootState['auth'];
    const requestHeaders: any = {};
    if (access_token) {
      requestHeaders.Authorization = `Bearer ${access_token}`;
    }
    requestHeaders['User-Agent'] = 'mobile';
    return this.axiosInstance({
      ...config,
      headers: {
        ...requestHeaders,
        ...config.headers,
      },
      method: config.method || 'GET',
      url: `http://192.168.32.104:3000/${endpoint}`,
    });
  }
}
