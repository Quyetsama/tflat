import {RequestService} from '@services/request.service';

class ApiService extends RequestService {
  async signIn(username: string, password: string) {
    const {data} = await this.callApi('api/auth/login', {
      method: 'POST',
      data: {
        loginField: username.toLowerCase(),
        password,
      },
    });
    return data;
  }

  async signUp(username: string, email: string, password: string) {
    const {data} = await this.callApi('api/auth/register', {
      method: 'POST',
      data: {
        username,
        email,
        password,
        confirmPassword: password,
        recaptcha: '123456789',
      },
    });
    return data;
  }

  async bookmarks() {
    const {data} = await this.callApi('bookmarks');
    return data;
  }

  async addBookmark(_id: string) {
    const {data} = await this.callApi(`bookmarks/new/${_id}`, {
      method: 'POST',
    });
    return data;
  }

  async removeBookmark(_id: string) {
    const {data} = await this.callApi(`bookmarks/${_id}`, {
      method: 'DELETE',
    });
    return data;
  }

  async search(query: string) {
    const {data} = await this.callApi('words', {
      params: {word: query, page: 1, limit: 10},
    });
    return data;
  }

  async wordDetails(_id: string) {
    const {data} = await this.callApi(`words/${_id}`);
    return data;
  }

  async getExams() {
    const {data} = await this.callApi(`exams`);
    return data;
  }

  async getExamDetails(_id: string) {
    const {data} = await this.callApi(`exams/${_id}`);
    return data;
  }

  async finishExam(examId: string, answersId: string[]) {
    const {data} = await this.callApi(`user-exams/`, {
      method: 'POST',
      data: {
        examId,
        answersId,
      },
    });
    return data;
  }
}

export default new ApiService();
