import {combineReducers, configureStore} from '@reduxjs/toolkit';
import {TypedUseSelectorHook, useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {createBlacklistFilter, createFilter} from 'redux-persist-transform-filter';
import {
  persistReducer,
  persistStore,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist';
import {setAutoFreeze} from 'immer';

import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import {appSlice} from '@redux/slices/app.slice';
import {authSlice} from '@redux/slices/auth.slice';

setAutoFreeze(false);

export const reducers = combineReducers({
  app: appSlice.reducer,
  auth: authSlice.reducer,
});

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  timeout: 100000,
  whitelist: ['app', 'auth'],
  stateReconciler: autoMergeLevel2,
  transforms: [
    createFilter('app', []),
    createBlacklistFilter('auth', [
      'bookmarks',
    ]),
  ],
};
const persistedReducer = persistReducer<ReturnType<typeof reducers>>(persistConfig, reducers);

const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
      immutableCheck: false,
    }),
});

const persistor = persistStore(store);

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch;

export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

export {persistor, store};
