import {createAsyncThunk} from '@reduxjs/toolkit';
import {RootState} from '@redux/store';
import ApiService from '@services/api.service';

interface SignInWithPassword {
  username: string;
  password: string;
}

export const doSignInWithPassword = createAsyncThunk<
  any,
  SignInWithPassword,
  { state: RootState }
>('auth/sign-in', async (data, thunkAPI) => {
  return await ApiService.signIn(
    data.username,
    data.password,
  );
});
