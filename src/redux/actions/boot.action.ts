import {createAsyncThunk} from '@reduxjs/toolkit';
import {RootState} from '@redux/store';
import ApiService from "@services/api.service";

export const bootApp = createAsyncThunk<
  void,
  void,
  { state: RootState }
>('app/boot', async (_, thunkAPI) => {
  thunkAPI.dispatch(loadBookmarks());
});

export const loadBookmarks = createAsyncThunk<
  any,
  void,
  { state: RootState }
>('app/bookmarks', async (_, thunkAPI) => {
  return await ApiService.bookmarks()
});
