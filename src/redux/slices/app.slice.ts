import {createSlice} from '@reduxjs/toolkit';

export interface AppSliceState {
  booted: boolean;
}

const initialData: AppSliceState = {
  booted: false,
};

export const appSlice = createSlice({
  name: 'app',
  initialState: initialData,
  reducers: {

  },
});

export const {} = appSlice.actions;
