import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {doSignInWithPassword} from "@redux/actions/auth.action";
import {loadBookmarks} from "@redux/actions/boot.action";

export interface AuthSliceState {
  isSignedIn: boolean;
  access_token: string;
  refresh_token: string;
  access_token_expires_in: number;
  account: Account;
  bookmarks: Array<any>;
}

const initialData: AuthSliceState = {
  isSignedIn: false,
  access_token: '',
  refresh_token: '',
  access_token_expires_in: 0,
  account: {
    _id: '',
    username: '',
    email: '',
    avatar: '',
    displayName: '',
  },
  bookmarks: [],
};

export const authSlice = createSlice({
  name: 'auth',
  initialState: initialData,
  reducers: {
    setLogout(state) {
      return initialData;
    },
  },
  extraReducers: builder => {
    builder
      .addCase(doSignInWithPassword.fulfilled, (state, action: PayloadAction<any>) => {
        const {access_token, refresh_token, access_token_expires_in, account} = action.payload;
        console.log(action.payload);
        state.access_token = access_token;
        state.refresh_token = refresh_token;
        state.access_token_expires_in = access_token_expires_in;
        state.account = account;
        state.isSignedIn = true;
      })
      .addCase(doSignInWithPassword.rejected, (state, action: PayloadAction<any>) => {
        console.log('doSignInWithPassword rejected');
      })
      .addCase(loadBookmarks.fulfilled, (state, action: PayloadAction<any>) => {
        state.bookmarks = action.payload;
      });
  },
});

export const {setLogout} = authSlice.actions;
